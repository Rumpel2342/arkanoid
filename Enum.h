#pragma once

enum Reaction {
	NOTHING,
	ToDELETE,
	Collision
};

enum GameStatus {
	Play, 
	Test,
	ResultWin
};

enum EnumBonus {
	Free = -1,
	NoBonus = 0,
	Trampoline = 1,
	ExtraLife = 2,
	SpeedUp = 3,
	SpeedDown = 4,
	PaddleMore = 5,
	PaddleLess = 6,
	Sticking = 7,
	ExtraBall = 8,
	BallSizeUp = 9,
	BallSpeedSetUp = 10
};


