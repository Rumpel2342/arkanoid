#include "BlockWithBonus.h"

BlockWithBonus::BlockWithBonus(sf::RenderWindow * window, float x, float y, float width, float height) :
	Block(window, x, y, width, height) { 
	this->rectangle.setFillColor(sf::Color::Magenta);
	this->numOfHits = 1;
}

EnumBonus BlockWithBonus::getBonus() {
	return EnumBonus(rand() % 11);
}

BlockWithBonus::~BlockWithBonus() { }
