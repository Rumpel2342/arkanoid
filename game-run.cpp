#include "SFML/Graphics.hpp"
#include <ctime> 

#include "ArcBlocks.h"
#include "game-run.h"

void showScores(RenderWindow& window, int lifes ,int score, String& bonusMessage) {
	Font font;
	font.loadFromFile("CyrilicOld.TTF");
	Text text("", font, 20);
	text.setPosition(50, 50);
	text.setString("Lifes: " + std::to_string(lifes) + "\nscore: " + std::to_string(score) + "\n" + bonusMessage);
	window.draw(text);
}

void generateLevel(RenderWindow& window, std::vector<Block*>& blocks, int row, int col, int begX,int begY ,int colStep, int rowStep) {
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			int BlockInterval = rand() % 100;
			if (BlockInterval >= 0 && BlockInterval < 10) {
				blocks.push_back(new IndestructibleBlock(&window, 100 + colStep * i, 200 + rowStep * j, 30, 15));
			}
			else if (BlockInterval >= 10 && BlockInterval < 30) {
				blocks.push_back(new FlickeringBlock(&window, 100 + colStep * i, 200 + rowStep * j, 30, 15));
			}
			else if (BlockInterval >= 30 && BlockInterval < 50) {
				blocks.push_back(new BlockWithBonus(&window, 100 + colStep * i, 200 + rowStep * j, 30, 15));
			}
			else if (BlockInterval >= 50 && BlockInterval < 100) {
				blocks.push_back(new MultiHitBlock(&window, 100 + colStep * i, 200 + rowStep * j, 30, 15));
			}
		}
	}
}

GameStatus runGame(RenderWindow& window, std::vector<Ball*>& balls, std::vector<Block*>& blocks, Paddle& paddle, int& numOfLives, int& score) {
	sf::Vertex line[2]= {sf::Vertex(sf::Vector2f(0,150)), sf::Vertex(sf::Vector2f(window.getSize().x,150))};
	window.draw(line, 2, sf::Lines);
	Event event;
	static String answer = "";
	auto gs = Play;
	auto prev = time(NULL);
	while (window.pollEvent(event)){
		if (event.type == Event::Closed || 
			(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)) {
			gs = ResultWin;
		}
	}

	//check pairs of ball to find collision
	for (int i = 0, s = balls.size() - 1; i < s - 1; i++) {
		for (int j = i + 1; j < s; j++) {
			balls[i]->checkCollision(*balls[j]);
		}
	}

	//check ball`s collision with padle and check for restore size
	for (auto ball : balls) {
		ball->draw();
		if (paddle.checkCollision(*ball) > 0 ){
			balls.erase(std::find(balls.begin(), balls.end(), ball));
			paddle.addBall(ball);
		}
		if (ball->Increazed != 0 && time(NULL) - ball->Increazed > 7) {
			ball->changeSize(0.64);
		}
		if (ball->update() == ToDELETE) {
			balls.erase(std::find(balls.begin(), balls.end(), ball));
			numOfLives--;
			delete ball;
		}
	}

	//check ball`s collision with blocks and get bonuses if its posible
	for (Block* block : blocks) {
		block->draw();
		for (Ball* ball : balls) {
			int num;
			if (block->checkCollision(*ball) == Collision) {
				switch (block->getBonus()) {
					case Trampoline:
						answer = "add trampoline\n";
						Ball::onTrampoline();
						break;
					case ExtraLife:
						answer = "add extra life\n";
						numOfLives++;
						break;
					case SpeedUp:
						answer = "speed up\n";
						ball->setV(ball->vx * 2, ball->vy * 2);
						break;
					case SpeedDown:
						answer = "speed down\n";
						ball->setV(ball->vx /= 2, ball->vy /= 2);
						break;
					case PaddleMore:
						answer = "paddle size up\n";
						paddle.setSize(2);
						break;
					case PaddleLess:
						answer = "paddle size down\n";
						paddle.setSize(0.5);
						break;
					case Sticking:
						num = rand() % 3 + 1;
						answer = "add" + std::to_string(num) + "tries\n";
						paddle.addCatchTry(num);
						break;
					case ExtraBall:
						answer = "add one ball\n";
						balls.push_back(new Ball(&window, ball->r, -ball->vx, - ball->vy, ball->x + ball->r, ball->y + ball->r));
						break;
					case BallSizeUp:
						answer = "ball size increased for 7 sec\n";
						ball->changeSize(1.5625);
						break;
					case BallSpeedSetUp:
						ball->SinusTime  = time(NULL);
						break;
				}
			}
			if (block->getNumOfNits() == 0) {
				blocks.erase(std::find(blocks.begin(), blocks.end(), block));
				score += block->getScore();
				delete block;
				break;
			}
		}
	}

	//if paddle throw smth
	Ball* newBall;
	if ((newBall = paddle.action()) != nullptr) { balls.push_back(newBall); }

	//if you lose all balls, but you have extra life
	paddle.draw();
	if (balls.size() <= 0 && paddle.haveBalls() <= 0 && numOfLives > 0) { 
		numOfLives--;
		paddle.addBall();
	}

	//if you lose
	if (numOfLives <= 0) { 
		for (auto ball : balls) {delete ball;}
		for (auto block : blocks) {delete block;}
		gs = ResultWin;
	}

	showScores(window, numOfLives, score, answer);
	return gs;
}

void runResultWin(RenderWindow& window, int score) {
	Event event;
	Font font;
	font.loadFromFile("CyrilicOld.TTF");
	Text text("", font, 20);
	text.setPosition(200, 200);
	text.setString("Game over\nYour score is: " + std::to_string(score) + "\npress Escape to leave the Game");
	while (window.pollEvent(event)) {
		if (event.type == Event::Closed ||
			(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)) {
			window.close();
		}
	}
	window.draw(text);
}