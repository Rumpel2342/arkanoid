#include <SFML/Graphics.hpp>
#include <time.h>
#include <vector>
#include <algorithm>
#include <chrono>
#include <ctime>    

#include "Paddle.h"
#include "Ball.h"
#include "Block.h"
#include "FlickeringBlock.h"
#include "BlockWithBonus.h"
#include "Enum.h"

#include "game-run.h"
using namespace sf;
bool Ball::trap = false;

int main() {
	Event loseEvent;
	int numOfBalls = 1;
	int blockRow = 15;
	int blockCol = 3;
	int score = 0; 
	int numOfLives = 1;
	GameStatus gamestatus = Play;
	String result = "";

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	srand(time(0));
	RenderWindow window(VideoMode(900, 600), "Arkanoid", sf::Style::Default, settings);
	window.setFramerateLimit(30);
	Paddle paddle(&window, window.getSize().x/2);
	std::vector<Ball*> balls(0);
	std::vector<Block*> blocks(0);
	generateLevel(window, blocks, blockRow, blockCol, 100, 200, 50, 50);
	//game Loop
	while (window.isOpen()){
		window.clear();
		switch (gamestatus) {
		case Play: 
				gamestatus = runGame(window, balls, blocks, paddle, numOfLives, score);
				break;
		case ResultWin:
				runResultWin(window, score);
				break;
		}
		window.display();
	}
	return 0;
}
