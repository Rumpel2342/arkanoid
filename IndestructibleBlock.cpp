#include "IndestructibleBlock.h"



IndestructibleBlock::IndestructibleBlock(sf::RenderWindow * window, float x, float y, float width, float height) :
	Block(window, x, y, width, height){
	this->rectangle.setFillColor(sf::Color(71, 74, 81)); // grey
	this->numOfHits = -1;
}

IndestructibleBlock::~IndestructibleBlock() { }
