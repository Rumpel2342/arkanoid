#include "Block.h"
#include "utils.h"

Block::Block(sf::RenderWindow * window, float x, float y, float width, float height) { 
	this->window = window;
	this->x = x;
	this->y = y;
	this->numOfHits = 0;
	this->rectangle.setSize(sf::Vector2f(width, height));
	this->rectangle.setOrigin(this->rectangle.getSize().x/2, this->rectangle.getSize().y/2);
	this->rectangle.setPosition(this->x, this->y);
}

Reaction Block::checkCollision(Ball & ball) { 
	sf::RectangleShape* block = this->getBlock();
	if (!(ball.getBall()->getGlobalBounds().intersects(block->getGlobalBounds()))) { return NOTHING; }
	changeColorOnHit();
	sf::Vector2f ld(block->getGlobalBounds().left, 
									block->getGlobalBounds().top + block->getGlobalBounds().height);
	sf::Vector2f lu(block->getGlobalBounds().left, 
									block->getGlobalBounds().top);
	sf::Vector2f rd(block->getGlobalBounds().left + block->getGlobalBounds().width, 
									block->getGlobalBounds().top + block->getGlobalBounds().height);
	sf::Vector2f ru(block->getGlobalBounds().left + block->getGlobalBounds().width, 
									block->getGlobalBounds().top);
	sf::Vector2f circleCenter(ball.x, ball.y);
	// horizontal blow
	if (ball.y <= ld.y && ball.y >= lu.y){
		ball.vx = -ball.vx;
		if (ball.x < ld.x) { ball.x -= 1; }
		else if (ball.x > rd.x) { ball.x += 1; }
	}
	// vertical blow
	else if (ball.x <= rd.x && ball.x >= ld.x){
		ball.vy = -ball.vy;
		if (ball.y < lu.y) { ball.y -= 1; }
		else if (ball.y > ld.y) { ball.y += 1; }
	}
	// angular blow
	//else if (getDistance(ld, circleCenter) < ball.r || getDistance(lu, circleCenter) < ball.r || 
	//					getDistance(rd, circleCenter) < ball.r || getDistance(ru, circleCenter) < ball.r) {
	//	if ((ball.vx > 0 && ball.vy > 0 ) || (ball.vx < 0 && ball.vy > 0)){ ball.setV(ball.vy, -ball.vx); }
	//	else if ((ball.vx > 0 && ball.vy < 0 ) || (ball.vx < 0 && ball.vy > 0)){ ball.setV(-ball.vy, ball.vx); }
	//	if (ball.y < ld.y && ball.x < lu.x) { 
	//		ball.y -= 1;
	//		ball.x -= 1;
	//	}
	//	else if (ball.y > lu.y && ball.x < lu.x) { 
	//		ball.y += 1;
	//		ball.x -= 1;
	//	}
	//	else if (ball.y > ru.y && ball.x > ru.x) { 
	//		ball.y += 1;
	//		ball.x += 1;
	//	}
	//	else if (ball.y < rd.y && ball.y > ru.x) { 
	//		ball.y -= 1;
	//		ball.x += 1;
	//	}
	//}
	else { return NOTHING; }
	if (this->numOfHits > 0){ this->numOfHits--; }
	ball.getBall()->setPosition(ball.x, ball.y);
	return Collision;
}

void Block::draw() { this->window->draw(this->rectangle); }

Block::~Block() { }
