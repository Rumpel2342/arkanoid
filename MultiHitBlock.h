#pragma once
#include "Block.h"

class MultiHitBlock : public Block {
protected:
	void changeColorOnHit();
public:
	MultiHitBlock(sf::RenderWindow * window, float x, float y, float width, float height);
	~MultiHitBlock();
};

