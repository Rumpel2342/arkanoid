#define _USE_MATH_DEFINES
#include <cmath>
#include "Paddle.h"
#include "utils.h"

Paddle::Paddle(sf::RenderWindow * window){
	this->window = window;
	this->x = this->window->getSize().x / 2;
	this->vx = 10;
	this->trapeze.setFillColor(sf::Color(100,0,0));
	trapeze.setPointCount(4);
	
	trapeze.setPoint(0, {100, 10});
	trapeze.setPoint(1, {-100, 10});
	trapeze.setPoint(2, {-40, -10});
	trapeze.setPoint(3, {40, -10});
	this->trapeze.setPosition(this->x, this->window->getSize().y - this->trapeze.getGlobalBounds().height / 2 -	20);
	this->catchedBalls.push_back(new Ball(window, 10, 0, 0, this->x, this->trapeze.getGlobalBounds().height));
	this->aim[0] = sf::Vertex(sf::Vector2f(this->x, trapeze.getPosition().y-this->trapeze.getGlobalBounds().height/2));
	this->aim[1] = sf::Vertex(sf::Vector2f(this->x, trapeze.getPosition().y-100));
	lastthrow = time(NULL);
}

Paddle::Paddle(sf::RenderWindow * window, float startpos): Paddle(window) {
	this->x = startpos;
	this->trapeze.setPosition(this->x, this->trapeze.getPosition().y);
}

void Paddle::addBall() { 
	this->catchedBalls.push_back(new Ball(window, 10, 0, 0, this->x, this->trapeze.getGlobalBounds().height));
}

void Paddle::addBall(Ball* ball) { 
	this->catchedBalls.push_back(ball);
}

void Paddle::setSize(float times) { 
	if (abs(this->getPaddle()->getPoint(0).x - this->getPaddle()->getPoint(1).x) <= 80 ||
			abs(this->getPaddle()->getPoint(0).x - this->getPaddle()->getPoint(1).x) >= this->window->getSize().x - 100) 
			{ return; }
	this->getPaddle()->setPoint(0, { (this->getPaddle()->getPoint(0).x) * 2, (this->getPaddle()->getPoint(0).y)});
	this->getPaddle()->setPoint(1, { (this->getPaddle()->getPoint(1).x) * 2, (this->getPaddle()->getPoint(1).y)});
	this->getPaddle()->setPoint(2, { (this->getPaddle()->getPoint(2).x) * 2, (this->getPaddle()->getPoint(2).y)});
	this->getPaddle()->setPoint(3, { (this->getPaddle()->getPoint(3).x) * 2, (this->getPaddle()->getPoint(3).y)});}

void Paddle::draw() {
	this->window->draw(this->trapeze);
	if (this->catchedBalls.size() > 0) {
		this->window->draw(this->aim, 2, sf::Lines);
	}
}

Ball* Paddle::action() {
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		if (this->x < this->window->getSize().x - this->trapeze.getGlobalBounds().width / 2) {
			this->x += vx;
			this->trapeze.move(vx, 0);
			this->aim[0].position.x += vx;
			this->aim[1].position.x += vx;
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		if (this->x > this->trapeze.getGlobalBounds().width / 2) {
			this->x -= vx;
			this->trapeze.move(-vx, 0);
			this->aim[0].position.x -= vx;
			this->aim[1].position.x -= vx;
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		if (catchedBalls.size() > 0) {
			float len = std::sqrt(std::pow(this->aim[0].position.x - this->aim[1].position.x , 2) + 
														std::pow(this->aim[0].position.y - this->aim[1].position.y , 2));
			float theta = acos((this->aim[1].position.x - this->aim[0].position.x) / len);
			if (theta > M_PI - 0.1) { return nullptr; }
			this->aim[1].position.x = this->aim[0].position.x + cos(theta + M_PI / 90) * len;
			this->aim[1].position.y = this->aim[0].position.y - sin(theta + M_PI / 90) * len;
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		if (catchedBalls.size() > 0) {
			float len = std::sqrt(std::pow(this->aim[0].position.x - this->aim[1].position.x , 2) + 
														std::pow(this->aim[0].position.y - this->aim[1].position.y , 2));
			float theta = acos((this->aim[1].position.x - this->aim[0].position.x) / len);
			if (theta < 0.1) { return nullptr; }
			this->aim[1].position.x = this->aim[0].position.x + cos(theta - M_PI / 90) * len;
			this->aim[1].position.y = this->aim[0].position.y - sin(theta - M_PI / 90) * len;
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter)) {
		if (catchedBalls.size() > 0 &&  time(NULL) - this->lastthrow > 1) {
			lastthrow = time(NULL);
			Ball* ball = catchedBalls[0];
			catchedBalls.erase(std::find(catchedBalls.begin(), catchedBalls.end(), ball));
			this->posibleCatch--;
			ball->x = this->aim[0].position.x;
			ball->y = this->aim[0].position.y - ball->r - 1;
			ball->setV((this->aim[1].position.x-this->aim[0].position.x)/double(20), (this->aim[1].position.y-this->aim[0].position.y)/double(20));
			return ball;
		}
	}
	return nullptr;
}

int Paddle::checkCollision(Ball & ball) { 
		sf::Vector2f pos = this->getPaddle()->getPosition();
		sf::Vector2f rd = this->getPaddle()->getPoint(0);
		sf::Vector2f ld = this->getPaddle()->getPoint(1);  
		sf::Vector2f lu = this->getPaddle()->getPoint(2);
		sf::Vector2f ru = this->getPaddle()->getPoint(3);
		rd.x += pos.x, rd.y+= pos.y;
		ld.x += pos.x, ld.y+= pos.y;
		lu.x += pos.x, lu.y+= pos.y;
		ru.x += pos.x, ru.y+= pos.y;
		if (ball.y + ball.r < lu.y) { 
			return 0; 
		}
		if (ball.x >= lu.x && ball.x <= ru.x){ 
			if (ball.vy < 0) { return 0; }
			ball.setV(ball.vx, -ball.vy);
		}
		else if (ball.x > ld.x && ball.x < lu.x ){ 
			//if (ball.vy > 0) { return 0; }
			sf::Vector2f v1(ball.x - lu.x, ball.y - lu.y);
			sf::Vector2f v2(lu.x - ld.x, lu.y - ld.y);
			sf::Vector2f ballPos(ball.x, ball.y);	
			float cosa = cosVec(v1,v2);
			float d = getDistance(ballPos, lu)*std::sqrt(1-cosa*cosa);
			if (d > ball.r) { 
				return 0; 
			}
			
			float nx = (lu.y - ld.y);
			float ny = (ld.x - lu.x);
			float scal = (ball.vx * nx + ball.vy * ny) / (nx*nx + ny*ny);
			ball.setV(ball.vx - 2.0 * nx * scal,ball.vy - 2.0 * ny * scal);
		}
		else if (ball.x > ru.x && ball.x < rd.x ){ 
			//if (ball.vy > 0) {return 0;}
			sf::Vector2f v1(ball.x - ru.x, ball.y - ru.y);
			sf::Vector2f v2(ru.x - rd.x, ru.y - rd.y);
			sf::Vector2f ballPos(ball.x, ball.y);
			float cosa = cosVec(v1, v2);
			float d = getDistance(ballPos, ru) * std::sqrt(1 - cosa * cosa);
			if (d > ball.r) {
				return 0;
			}
	
			float nx = (ru.y - rd.y);
			float ny = (rd.x - ru.x);
			float scal = (ball.vx * nx + ball.vy * ny) / (nx*nx + ny*ny);
			ball.vx -= 2.0 * nx *scal;
			ball.vy -= 2.0 * ny *scal;
		}
		return this->posibleCatch;
}

//int Paddle::checkCollision(Ball& ball) {
//	sf::Vector2f pos = this->getPaddle()->getPosition();
//	sf::Vector2f rd = this->getPaddle()->getPoint(0);
//	sf::Vector2f ld = this->getPaddle()->getPoint(1);
//	sf::Vector2f lu = this->getPaddle()->getPoint(2);
//	sf::Vector2f ru = this->getPaddle()->getPoint(3);
//	rd.x += pos.x, rd.y += pos.y;
//	ld.x += pos.x, ld.y += pos.y;
//	lu.x += pos.x, lu.y += pos.y;
//	ru.x += pos.x, ru.y += pos.y;
//	float legth =  ru.x - lu.x;
//	if (ball.y + ball.r < lu.y) { return 0; }
//	if (ball.x > lu.x  + legth /  3.0 && ball.x < ru.x - legth / 3.0) {
//		if (ball.vy < 0) { return 0; }
//		ball.setV(ball.vx, -ball.vy);
//	}
//	else if (ball.x >= lu.x && ball.y <= lu.x + legth /  3.0) {
//		if (ball.vy < 0) { return 0; }
//		sf::Vector2f v1(ball.x - lu.x, ball.y - lu.y);
//		sf::Vector2f v2(lu.x - ld.x, lu.y - ld.y);
//		sf::Vector2f ballPos(ball.x, ball.y);
//		sf::Vector2f ballVec(ball.vx, ball.vy);
//		if (ball.vx > 0){
//		}
//		float cosa = cosVec(v1, v2);
//		float d = getDistance(ballPos, lu) * std::sqrt(1 - cosa * cosa);
//		if (d > ball.r) { return 0; }
//
//		float nx = (lu.y - ld.y);
//		float ny = (ld.x - lu.x);
//		float scal = (ball.vx * nx + ball.vy * ny) / (nx * nx + ny * ny);
//		ball.setV(ball.vx - 2.0 * nx * scal, ball.vy - 2.0 * ny * scal);
//	}
//	else if (ball.x > ru.x && ball.x < rd.x) {
//		if (ball.vy < 0) { return 0; }
//		sf::Vector2f v1(ball.x - ru.x, ball.y - ru.y);
//		sf::Vector2f v2(ru.x - rd.x, ru.y - rd.y);
//		sf::Vector2f ballPos(ball.x, ball.y);
//		float cosa = cosVec(v1, v2);
//		float d = getDistance(ballPos, ru) * std::sqrt(1 - cosa * cosa);
//		if (d > ball.r) { return 0; }
//
//		float nx = (ru.y - rd.y);
//		float ny = (rd.x - ru.x);
//		float scal = (ball.vx * nx + ball.vy * ny) / (nx * nx + ny * ny);
//		ball.vx -= 2.0 * nx * scal;
//		ball.vy -= 2.0 * ny * scal;
//	}
//	return this->posibleCatch;
//}

Paddle::~Paddle() {
	for (auto ball : catchedBalls) { delete ball; }
}
