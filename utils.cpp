#include "utils.h"

float getDistance(sf::Vector2f & v1, sf::Vector2f & v2) noexcept { 
	return sqrt(pow((v2.x - v1.x), 2) + pow((v2.y - v1.y), 2));
}

float cosVec(sf::Vector2f & v1, sf::Vector2f & v2) {
	return ((v1.x)*(v2.x) + (v1.y)*(v2.y)) / (
		std::sqrt((v1.x)*(v1.x) + (v1.y)*(v1.y)) * 
		std::sqrt((v2.x)*(v2.x) + (v2.y)*(v2.y)));
}