#pragma once

#include <SFML/Graphics.hpp>
#include "Enum.h"

class Ball {
	sf::CircleShape ball;
	sf::RenderWindow* window;
	static bool trap;
	static void offTrampoline() { Ball::trap = false; }
	static bool getTrampoline() { return Ball::trap; }

public:
	float x;
	float y;
	float vx;
	float vy;
	float r;
	time_t Increazed;
	time_t SinusTime;

	Ball(sf::RenderWindow* window, float r);
	Ball(sf::RenderWindow* window, float r, float vx, float vy);
	Ball(sf::RenderWindow * window, float r, float vx, float vy, float x, float y);
	void draw();
	Reaction update();

	void changeSize(float times);
	void setV(float vx, float vy);
	void checkCollision(Ball& another);
	static void onTrampoline(){Ball::trap = true;}

	sf::CircleShape * getBall() { return &ball;}

	~Ball();
};

