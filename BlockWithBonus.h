#pragma once
#include "Block.h"

class BlockWithBonus : public Block {
public:
	BlockWithBonus(sf::RenderWindow * window, float x, float y, float width, float height);
	EnumBonus getBonus();
	int getScore() { return 2; }
	~BlockWithBonus();
};

