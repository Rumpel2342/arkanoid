#pragma once
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Ball.h"

class Paddle {
	float x;
	float vx;
	sf::ConvexShape trapeze;
	sf::RenderWindow* window;
	int posibleCatch = 1;
	std::vector<Ball*> catchedBalls;
	sf::Vertex aim[2];
	time_t lastthrow;

public:
	Paddle(sf::RenderWindow* window);
	Paddle(sf::RenderWindow* window, float startpos);

	~Paddle();

	sf::ConvexShape* getPaddle() {return &(trapeze); }
	int haveBalls() { return catchedBalls.size(); }
	int checkCollision(Ball & ball);
	void addBall();
	void addBall(Ball* ball);
	void addCatchTry(int num) {posibleCatch += num;}
	void setSize(float times);
	void draw();
	Ball* action();
};

