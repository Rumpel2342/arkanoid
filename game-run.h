#pragma once
#include <string>

#include "Enum.h"
#include "Ball.h"
#include "Paddle.h"
#include "ArcBlocks.h"

using namespace sf;
GameStatus runGame(RenderWindow& window, std::vector<Ball*>& balls, std::vector<Block*>& blocks, Paddle& paddle, int& numOfLives, int& score);
void generateLevel(RenderWindow& window, std::vector<Block*>& blocks, int row, int col, int begX, int begY, int colStep, int rowStep);
void runResultWin(RenderWindow& window, int score);
