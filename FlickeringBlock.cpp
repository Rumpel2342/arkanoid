#include <iostream>
#include "FlickeringBlock.h"
#include <ctime>    

FlickeringBlock::FlickeringBlock(sf::RenderWindow * window, float x, float y, float width, float height) :
	Block(window, x, y, width, height){
	this->numOfHits = 1;
	this->rectangle.setFillColor(sf::Color::White);
}

Reaction FlickeringBlock::checkCollision(Ball & ball) {
	if (time(NULL) % 10 > 4) {
		Block::checkCollision(ball);
	}
	return Reaction();
}

EnumBonus FlickeringBlock::getBonus() {
	return EnumBonus(rand() % 8);
}

FlickeringBlock::~FlickeringBlock() { }

void FlickeringBlock::draw() {
	if (time(NULL) % 10 > 4) {
		this->window->draw(this->rectangle);
	}
}