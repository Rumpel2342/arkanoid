#pragma once
#include "Bonus.h"
#include "Ball.h"
#include "Enum.h"
#include <SFML/Graphics.hpp>

class Block {
protected:
	float x;
	float y;
	int numOfHits;

	sf::RectangleShape rectangle;
	sf::RenderWindow* window;

	virtual void changeColorOnHit() { getBlock()->setFillColor(sf::Color::Black); };
public:
	Block(sf::RenderWindow * window, float x, float y, float width, float height);

	sf::RectangleShape* getBlock(){ return &(this->rectangle); }
	int getNumOfNits() { return numOfHits; }
	virtual Reaction checkCollision(Ball & ball);
	virtual EnumBonus getBonus(){ return NoBonus; };
	virtual int getScore(){ return 1;}

	virtual void draw();
	virtual ~Block();
};

