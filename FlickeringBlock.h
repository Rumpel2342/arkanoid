#pragma once
#include "Block.h"

class FlickeringBlock : public Block {
public:
	FlickeringBlock(sf::RenderWindow * window, float x, float y, float width, float height);
	Reaction checkCollision(Ball & ball);
	EnumBonus getBonus();
	~FlickeringBlock();
	int getScore() { return 3; }
	void draw();
};

