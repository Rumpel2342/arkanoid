#include "MultiHitBlock.h"

MultiHitBlock::MultiHitBlock(sf::RenderWindow * window, float x, float y, float width, float height):
	Block(window, x, y, width, height) {
	this->numOfHits = std::rand() % 3 + 1;
	switch (numOfHits) {
	case 3:
		this->rectangle.setFillColor(sf::Color::Green);
		break;
	case 2:
		this->rectangle.setFillColor(sf::Color::Yellow);
		break;
	case 1:
		this->rectangle.setFillColor(sf::Color::Red);
		break;
	}
}

MultiHitBlock::~MultiHitBlock() { }

void MultiHitBlock::changeColorOnHit() {
	if (getBlock()->getFillColor() == sf::Color::Green) {
		getBlock()->setFillColor(sf::Color::Yellow);
	}
	else if (getBlock()->getFillColor() == sf::Color::Yellow) {
		getBlock()->setFillColor(sf::Color::Red);
	}
	else if (getBlock()->getFillColor() == sf::Color::Red) {
		getBlock()->setFillColor(sf::Color::Black);
	}
}