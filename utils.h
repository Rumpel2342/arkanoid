#pragma once
#include <SFML/Graphics.hpp>

float getDistance(sf::Vector2f & v1, sf::Vector2f & v2) noexcept;
float cosVec(sf::Vector2f & v1, sf::Vector2f & v2);