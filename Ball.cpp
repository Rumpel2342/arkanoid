#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include "Ball.h"
#include "utils.h"

Ball::Ball(sf::RenderWindow * window, float r) { 
	this->window = window;
	this->r = r;
	this->x = this->window->getSize().x / 2;
	this->y = this->window->getSize().y / 2;
	this->vx = rand() % 6 - rand() % 3;
	this->vy = rand() % 6 - rand() % 5;
	this->ball = sf::CircleShape(r);
	this->ball.setOrigin(r,r );
	this->ball.setFillColor(sf::Color(100,100,0));
	this->ball.setPosition(this->x, this->y);
}

Ball::Ball(sf::RenderWindow * window, float r, float vx, float vy): Ball(window, r){
	this->vx = vx;
	this->vy = vy;
}

Ball::Ball(sf::RenderWindow * window, float r, float vx, float vy, float x, float y): Ball(window, r, vx, vy){
	this->x = x;
	this->y = y;
	this->ball.setPosition(this->x, this->y);
}

void Ball::draw(){
	this->window->draw(this->ball);
}

Reaction Ball::update()  {
	this->ball.setPosition(this->x, this->y);
	float sinus_dur = 20;

	if (this->SinusTime != 0 && time(NULL) - this->SinusTime < sinus_dur) {
		this->vx * cos((time(NULL) - this->SinusTime) / sinus_dur * M_PI * 2);
		this->vy * cos((time(NULL) - this->SinusTime) / sinus_dur * M_PI * 2);
	}
	else {
		this->SinusTime = 0;
	}

	this->x += vx;
	this->y += vy;
	this->ball.move(vx, vy);
	if (this->x  - this->r< 0 || this->x + this->r > this->window->getSize().x){
		x = x > this->window->getSize().x + this->r / 2? this->window->getSize().x : x;
		vx = -vx;
		if (this->x > this->window->getSize().x) {
			this->x -= 2;
		}
		else if (this->x < 0) {
			this->x += 2;
		}
	}
	if (this->y - this->r < 150){ 
		x = x < - this->r / 2? 0 : x;
		vy = -vy;
		if (this->y > this->window->getSize().y) {
			this->y -= 2;
		}
	}
	if (this->y + this->r > this->window->getSize().y) {
		if (Ball::getTrampoline()) {
			x = x > this->window->getSize().y + this->r / 2? 0 : x;
			vy = -vy;
			Ball::offTrampoline();
			return NOTHING;
		}
		return ToDELETE;
	}
	this->ball.setPosition(this->x, this->y);
	return NOTHING;
}

void Ball::changeSize(float times) {
	this->r *= times;
	if (times > 1) { this->Increazed = time(NULL);}
	else { this->Increazed = 0; }
	this->ball.setRadius(this->r);
}

void Ball::setV(float vx, float vy) {
	this->vx = vx;
	this->vy = vy;
}

void Ball::checkCollision(Ball& another) { 
	float dx = this->x - (another.x);
	float dy = this->y - (another.y);
	float d = std::sqrt(dx*dx + dy*dy);
	if (d < this->r + another.r) {
		float tmp = this->vx;
		this->vx = another.vx;
		another.vx = tmp;
		tmp = this->vy;
		this->vy = another.vy;
		another.vy = tmp;
		if (this->x > another.x) {
			this->x += 1;
		}
		else if (this->x < another.x) {
			this->x += 1;
		}
		else if (this->y > another.y) {
			this->y += 1;
		}
		else if (this->y < another.y) {
			this->y -= 1;
		}
	}
	this->ball.setPosition(this->x, this->y);
}

Ball::~Ball() { }