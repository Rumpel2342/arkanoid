#pragma once
#include "Block.h"

class IndestructibleBlock : public Block{
protected:
	void changeColorOnHit() { };
public:
	IndestructibleBlock(sf::RenderWindow * window, float x, float y, float width, float height);
	~IndestructibleBlock();
};

